package com.tgapi.Tgapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TgApiV2Application {

	public static void main(String[] args) {
		SpringApplication.run(TgApiV2Application.class, args);
	}

}
